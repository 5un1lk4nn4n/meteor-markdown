import React, { Component } from 'react';
import {Link} from 'react-router-dom';

import Account from './Accounts';


class Header extends Component {
  onBinClick(event){
    event.preventDefault();
    Meteor.call('bins.insert',(error,binId)=>{
        // Redirect to bin after creating
        return;
    });
  }
  render() {
    return (
      <nav className="nav navbar-default">
        <div className="navbar-header">
          <Link to="/" className="navbar-brand">Markbin</Link>
        </div>
        <ul className="nav navbar-nav">
          <li>
            <Account/>
          </li>
          <li>
            <a href="#" onClick={this.onBinClick.bind(this)}>Create Bin </a>
          </li>
        </ul>
      </nav>
    );
  }
}

export default Header;
