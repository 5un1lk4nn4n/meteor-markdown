import React from "react";
import ReactDOM from 'react-dom';
import { BrowserRouter as Router,Route, Switch } from 'react-router-dom';

import App from './components/App';
import BinsMain from './components/bins/bin_main';
import BinsList from './components/bins/bins_list';
import { Bins } from '../imports/collections/bins';

import './main.html';


const routes = (
  <Router>
    <App>
        <Switch>
          <Route path = "/bins/:binId" component = {BinsMain} />
          <Route path ="/" component = {BinsList} />
        </Switch>
    </App>
  </Router>
);

Meteor.startup(()=>{
  ReactDOM.render(routes,document.querySelector('.root'));

});
